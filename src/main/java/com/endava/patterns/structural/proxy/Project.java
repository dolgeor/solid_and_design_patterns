package com.endava.patterns.structural.proxy;

public interface Project {

    void run();
}
