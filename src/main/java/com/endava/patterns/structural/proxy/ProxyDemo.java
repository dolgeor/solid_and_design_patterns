package com.endava.patterns.structural.proxy;

public class ProxyDemo {

    public static void main(String[] args) {
        Project project = new ProxyProject("lololo");
        project.run();
        project.run();
    }
}
