package com.endava.patterns.structural.decorator;

public interface Coffee {

    double getCost();

    String getIngredients();

    default void printInfo() {
        System.out.println("Cost: " + getCost() + "; Ingredients: " + getIngredients());
    }
}
