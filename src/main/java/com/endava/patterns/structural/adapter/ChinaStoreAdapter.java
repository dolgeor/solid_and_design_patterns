package com.endava.patterns.structural.adapter;

import static com.endava.patterns.structural.adapter.ChinaSize.*;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ChinaStoreAdapter implements EuropeanStore {

    private final ChinaStore chinaStore;

    @Override
    public void buyShirt(final int europeanSize) {
        chinaStore.buyShirt(transformToChinaSize(europeanSize));
    }

    private ChinaSize transformToChinaSize(final int europeanSize) {
        switch (europeanSize) {
            case 36: return S;
            case 38: return M;
            case 40: return L;
        }
        return null;
    }
}
