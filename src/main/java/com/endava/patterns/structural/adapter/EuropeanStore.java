package com.endava.patterns.structural.adapter;

public interface EuropeanStore {

    void buyShirt(int europeanSize);
}
