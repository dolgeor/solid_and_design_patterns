package com.endava.patterns.structural.adapter;

public class AdapterDemo {

    public static void main(String[] args) {
       final EuropeanStore europeanStore = new ChinaStoreAdapter(new ChinaStore());
       europeanStore.buyShirt(38);
    }
}
