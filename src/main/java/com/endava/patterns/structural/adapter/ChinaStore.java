package com.endava.patterns.structural.adapter;

public class ChinaStore {

    public void buyShirt(final ChinaSize chinaSize) {
        System.out.println("Buying shirt from China, size => " + chinaSize);
    }
}
