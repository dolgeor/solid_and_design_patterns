package com.endava.patterns.structural.adapter;

public enum ChinaSize {
    S,
    M,
    L;
}
