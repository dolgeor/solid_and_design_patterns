package com.endava.patterns.structural.bridge;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public abstract class Shape {

    protected Color color;

    public abstract void applyColor();
}
