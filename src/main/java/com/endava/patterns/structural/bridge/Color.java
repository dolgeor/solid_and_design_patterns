package com.endava.patterns.structural.bridge;

public interface Color {

    void applyColor();
}
