package com.endava.patterns.structural.bridge;

public class GreenColor implements Color {

    public void applyColor() {
        System.out.println("green.");
    }
}