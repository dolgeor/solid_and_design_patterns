package com.endava.patterns.behavioral.state;

public class StateDemo {

    public static void main(String[] args) {
        Turnstile turnstile = new Turnstile();

        System.out.println("Initial state: " + turnstile.getCurrentState() + "\n");

        turnstile.push();
        turnstile.push();

        turnstile.coin();
        turnstile.coin();
        turnstile.coin();

        turnstile.push();
    }

}
