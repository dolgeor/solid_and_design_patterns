package com.endava.patterns.behavioral.state;

import lombok.Getter;

@Getter
public class Turnstile {

    private TurnstileState currentState;

    public Turnstile() {
        this.currentState = new LockedTurnstileState();
    }

    public void push() {
        System.out.println("Pushing >>>");
        TurnstileState state = this.currentState;
        currentState = this.currentState.push();
        if (!state.equals(currentState)) {
            System.out.println(this.currentState);
            System.out.println();
        }
    }

    public void coin() {
        System.out.println("Putting coin $$$");
        TurnstileState state = this.currentState;
        currentState = this.currentState.coin();
        if (!state.equals(currentState)) {
            System.out.println(this.currentState);
            System.out.println();
        }
    }

}
