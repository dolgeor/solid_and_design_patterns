package com.endava.patterns.behavioral.state;

public class UnlockedTurnstileState implements TurnstileState {

    @Override
    public TurnstileState coin() {
        return this;
    }

    @Override
    public TurnstileState push() {
        return new LockedTurnstileState();
    }

    @Override
    public String toString() {
        return "Unlocked";
    }
}
