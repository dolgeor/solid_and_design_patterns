package com.endava.patterns.behavioral.state;

public interface TurnstileState {

    TurnstileState coin();

    TurnstileState push();
}
