package com.endava.patterns.behavioral.state;

public class LockedTurnstileState implements TurnstileState {

    @Override
    public TurnstileState coin() {
        return new UnlockedTurnstileState();
    }

    @Override
    public TurnstileState push() {
        return this;
    }

    @Override
    public String toString() {
        return "Locked";
    }
}
