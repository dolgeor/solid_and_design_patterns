package com.endava.patterns.behavioral.observer;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Subscriber {

    private String name;

    public void receiveNotification(final String notification) {
        System.out.println(notification);
    }
}
