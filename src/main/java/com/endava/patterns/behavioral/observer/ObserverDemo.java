package com.endava.patterns.behavioral.observer;

import java.util.ArrayList;

public class ObserverDemo {

    public static void main(String[] args) {
        YouTubeChannel channel = new YouTubeChannel(new ArrayList<>());

        final Subscriber subscriber1 = new Subscriber("Jon");
        final Subscriber subscriber2 = new Subscriber("Pit");
        final Subscriber subscriber3 = new Subscriber("George");

        channel.addSubscriber(subscriber1);
        channel.addSubscriber(subscriber2);
        channel.addSubscriber(subscriber3);

        channel.publishNewVideo("Solid tutorial");
    }
}
