package com.endava.patterns.behavioral.chain;

import java.time.LocalDate;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class WithdrawRequest {

    private String pin;

    private LocalDate expirationDate;

    private Integer amount;

}
