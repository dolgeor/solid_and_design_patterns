package com.endava.patterns.behavioral.chain;

public interface Validator {

    void validate(final WithdrawRequest request);
}
