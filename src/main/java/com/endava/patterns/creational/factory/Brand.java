package com.endava.patterns.creational.factory;

public enum Brand {
    HONDA,
    MERCEDES,
    TOYOTA;
}
