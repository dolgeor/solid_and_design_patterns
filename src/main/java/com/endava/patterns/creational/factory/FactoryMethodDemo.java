package com.endava.patterns.creational.factory;

import java.util.Arrays;

public class FactoryMethodDemo {

    public static void main(String[] args) {
        Arrays.stream(Brand.values())
                .map(CarFactory::getCar)
                .forEach(Car::drive);
    }
}
