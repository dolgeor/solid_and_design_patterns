package com.endava.patterns.creational.factory.cars;

import com.endava.patterns.creational.factory.Car;

public class Toyota implements Car {

    @Override
    public void drive() {
        System.out.println("Driving Lexus");
    }
}