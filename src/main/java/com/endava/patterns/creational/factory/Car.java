package com.endava.patterns.creational.factory;

public interface Car {
    void drive();
}
