package com.endava.patterns.creational.factory;

import com.endava.patterns.creational.factory.cars.Honda;
import com.endava.patterns.creational.factory.cars.Mercedes;
import com.endava.patterns.creational.factory.cars.Toyota;

public class CarFactory {

    public static Car getCar(Brand brand) {
        switch (brand) {
            case HONDA:
                return new Honda();
            case TOYOTA:
                return new Toyota();
            case MERCEDES:
                return new Mercedes();
            default:
                throw new IllegalArgumentException("Unknown brand");
        }
    }
}
