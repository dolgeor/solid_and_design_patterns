package com.endava.solid.isp;

import lombok.AllArgsConstructor;

interface Worker extends Feedable, Workable {

}

interface Workable {

    void work();
}

interface Feedable {

    void eat();
}

public class InterfaceSegregationDemo {

    public static void main(String[] args) {
        // final Workable worker = new Robot();
        final Workable worker = new SuperWorker();
        final Manager manager = new Manager(worker);
        manager.manage();
    }
}

class SuperWorker implements Worker {

    @Override
    public void work() {
        System.out.println("SuperWorker is working hard");
    }

    @Override
    public void eat() {
        System.out.println("SuperWorker is eating much more");
    }
}

class Robot implements Workable {

    @Override
    public void work() {
        System.out.println("Robot is working hard");
    }
}

@AllArgsConstructor
class Manager {

    private Workable workable;

    public void manage() {
        workable.work();
    }
}
