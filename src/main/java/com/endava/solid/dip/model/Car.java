package com.endava.solid.dip.model;

import lombok.Data;

@Data
public class Car {

    private final String model;

    private final int year;
}
