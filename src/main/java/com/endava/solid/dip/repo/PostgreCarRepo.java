package com.endava.solid.dip.repo;

import java.util.Collections;
import java.util.List;

import com.endava.solid.dip.model.Car;
import com.endava.solid.dip.service.CarRepo;

public class PostgreCarRepo implements CarRepo {

    @Override
    public Car addCar(final Car car) {
        //add to db
        return car;
    }

    @Override
    public List<Car> getCarsByYear(final int year) {
        //getFrom db
        return Collections.emptyList();
    }
}
