package com.endava.solid.dip.repo;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.endava.solid.dip.model.Car;
import com.endava.solid.dip.service.CarRepo;

public class InMemoryCarRepo implements CarRepo {

    private List<Car> cars = new ArrayList<>();

    @Override
    public Car addCar(final Car car) {
        cars.add(car);
        return car;
    }

    @Override
    public List<Car> getCarsByYear(final int year) {
        return cars.stream().filter(car -> year == car.getYear()).collect(Collectors.toList());
    }
}
