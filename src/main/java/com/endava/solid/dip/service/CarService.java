package com.endava.solid.dip.service;

import java.util.List;

import com.endava.solid.dip.model.Car;

import lombok.Data;

@Data
public class CarService {

    private final CarRepo carRepo;

    public Car addCar(final Car car) {
        return carRepo.addCar(car);
    }

    public List<Car> getCarsByYear(final int year) {
        return carRepo.getCarsByYear(year);
    }
}
