package com.endava.solid.dip.service;

import java.util.List;

import com.endava.solid.dip.model.Car;

public interface CarRepo {

    Car addCar(final Car car);

    List<Car> getCarsByYear(final int year);

}
