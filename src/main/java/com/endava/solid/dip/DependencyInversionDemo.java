package com.endava.solid.dip;

import com.endava.solid.dip.model.Car;
import com.endava.solid.dip.repo.InMemoryCarRepo;
import com.endava.solid.dip.service.CarRepo;
import com.endava.solid.dip.service.CarService;

public class DependencyInversionDemo {

    public static void main(String[] args) {
        final CarRepo carRepo = new InMemoryCarRepo();
        final CarService carService = new CarService(carRepo);
        carService.addCar(new Car("bmw", 2015));
        carService.getCarsByYear(2015).forEach(System.out::println);
    }
}
